var numSquares = 6;
var colors = generateRandomColors(numSquares);
var squares = document.querySelectorAll(".square");
var pickedColor = pickColor();
var colorDisplay = document.getElementById("colorDisplay");
var messageDisplay = document.querySelector("#message");
var h1 = document.querySelector("h1");
var button = document.querySelector("#reset");
var modeButtons = document.querySelectorAll(".mode");

for (var i = 0; i < modeButtons.length; i++) {
    modeButtons[i].addEventListener("click", function () {
        modeButtons[0].classList.remove("selected");
        modeButtons[1].classList.remove("selected");
        this.classList.add("selected");

        //this.textContent === "Easy" ? numSquares = 3: numSquares = 6;
        if (this.textContent === "Easy") {
            numSquares = 3;
        } else {
            numSquares = 6;
        }
        reset();
    });

}

function reset() {
    colors = generateRandomColors(numSquares);
    pickedColor = pickColor();
    colorDisplay.textContent = pickedColor;
    messageDisplay.textContent = "";
    button.textContent = "New colors";
    for (var i = 0; i < squares.length; i++) {
        if (colors[i]) {
            squares[i].style.display = "block";
            squares[i].style.background = colors[i];
        } else {
            squares[i].style.display = "none";
        }

    }
    h1.style.background = "RGB(108, 159, 106)";
}

// easyBtn.addEventListener("click", function () {
//     hardBtn.classList.remove("selected");
//     easyBtn.classList.add("selected");
//     numSquares = 3;
//     colors = generateRandomColors(numSquares);
//     pickedColor = pickColor();
//     colorDisplay.textContent = pickedColor;
//     for (var i = 0; i < squares.length; i++) {
//         if (colors[i]) {
//             squares[i].style.background = colors[i];
//         } else {
//             squares[i].style.display = "none";
//         };
//     };
// });

// hardBtn.addEventListener("click", function () {
//     easyBtn.classList.remove("selected");
//     hardBtn.classList.add("selected");
//     numSquares = 6;
//     colors = generateRandomColors(numSquares);
//     pickedColor = pickColor();
//     colorDisplay.textContent = pickedColor;
//     for (var i = 0; i < squares.length; i++) {
//         squares[i].style.background = colors[i];
//         squares[i].style.display = "block";
//     };
// });


button.addEventListener("click", function () {
    reset();
    // //generate random colors
    // colors = generateRandomColors(numSquares);
    // //pick a new random color from array 
    // pickedColor = pickColor();
    // colorDisplay.textContent = pickedColor;
    // messageDisplay.textContent = "";
    // this.textContent = "New colors";
    // //change colors of squares 
    // for (var i = 0; i < squares.length; i++) {
    //     squares[i].style.background = colors[i];
    // };
    // h1.style.background = "RGB(108, 159, 106)";
});

colorDisplay.textContent = pickedColor;

for (var i = 0; i < squares.length; i++) {
    // add initial colors to squares
    squares[i].style.background = colors[i];

    // add event listener to squares 
    squares[i].addEventListener("click", function () {
        //grab clicked square color
        var clickedColor = this.style.background;
        if (clickedColor === pickedColor) {
            messageDisplay.textContent = "Correct!";
            button.textContent = "Play again?";
            changeColors(clickedColor);
            h1.style.background = clickedColor;

        } else {
            this.style.background = "rgba(22, 22, 28, 0.88)";
            messageDisplay.textContent = "Try again!";

        }

    });
}

function changeColors(color) {
    for (var i = 0; i < squares.length; i++) {
        squares[i].style.background = color;
    }
}

function pickColor() {
    var random = Math.floor(Math.random() * colors.length);
    return colors[random];

}
function generateRandomColors(num) {
    //make array;
    var arr = [];
    //add a num random colors to a aray
    for (var i = 0; i < num; i++) {
        arr.push(randomColor());
    }
    //return array;
    return arr;
}

function randomColor() {
    //pick a "red" color beetwen 0 to 255
    var r = Math.floor(Math.random() * 256);
    //pick a "green" color beetwen 0 to 255
    var g = Math.floor(Math.random() * 256);
    //pick a "blue" color beetwen 0 to 255
    var b = Math.floor(Math.random() * 256);
    return "rgb(" + r + ", " + g + ", " + b + ")";
}

